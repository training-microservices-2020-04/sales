package com.artivisi.training.microservices.sales.controller;

import com.artivisi.training.microservices.sales.dao.SalesDao;
import com.artivisi.training.microservices.sales.dto.ProductDto;
import com.artivisi.training.microservices.sales.entity.Sales;
import com.artivisi.training.microservices.sales.entity.SalesDetail;
import com.artivisi.training.microservices.sales.service.CatalogBackendService;
import com.artivisi.training.microservices.sales.service.SalesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/sales")
@Slf4j
public class SalesController {

    @Autowired private SalesDao salesDao;
    @Autowired private CatalogBackendService catalogBackendService;
    @Autowired private SalesService salesService;

    @GetMapping("/{id}")
    public Sales findById(@PathVariable String id) {
        log.info("Mencari data sales dengan id {}", id);
        Sales sales = salesDao.findById(id).orElse(null);

        if (sales != null) {
            for (SalesDetail sd : sales.getSalesDetails()) {
                String productCode = sd.getProductCode();
                ProductDto product = catalogBackendService.findProductById(productCode);
                sd.setProductDetail(product);
            }
        }

        return sales;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody @Valid Sales sales) {
        salesService.saveSales(sales);
    }

}
