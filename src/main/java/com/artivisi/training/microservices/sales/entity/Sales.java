package com.artivisi.training.microservices.sales.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Sales {
    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull
    private LocalDateTime transactionTime;

    @NotEmpty @NotNull
    private String salesNumber;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_customer")
    private Customer customer;

    @JsonManagedReference
    @OneToMany(mappedBy = "sales", orphanRemoval = true)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<SalesDetail> salesDetails = new ArrayList<>();

    public BigDecimal getTotalAmount() {
        BigDecimal total = BigDecimal.ZERO;

        for (SalesDetail sd : salesDetails) {
            total = total.add(sd.getUnitPrice().multiply(new BigDecimal(sd.getQuantity())));
        }

        return total;
    }
}
