package com.artivisi.training.microservices.sales.entity;

import com.artivisi.training.microservices.sales.dto.ProductDto;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity @Data @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SalesDetail {
    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @JsonBackReference
    @ManyToOne @NotNull
    @JoinColumn(name = "id_sales")
    private Sales sales;

    @NotEmpty
    private String productCode;

    @NotNull
    private BigDecimal unitPrice = BigDecimal.ZERO;

    @NotNull
    private Integer quantity;

    @Transient
    private ProductDto productDetail;
}
