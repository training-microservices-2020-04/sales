package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dto.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "catalog", fallback = CatalogBackendServiceFallback.class)
public interface CatalogBackendService {
    @GetMapping("/api/product/{id}")
    ProductDto findProductById(@PathVariable String id);
}
