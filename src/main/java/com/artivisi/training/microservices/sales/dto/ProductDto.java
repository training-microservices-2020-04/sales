package com.artivisi.training.microservices.sales.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {
    private String code;
    private String name;
    private BigDecimal price;
    private String backendSource;
}
