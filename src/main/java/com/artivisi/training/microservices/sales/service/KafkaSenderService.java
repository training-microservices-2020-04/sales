package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dto.InvoiceRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service @Slf4j
public class KafkaSenderService {

    @Value("${kafka.topic.invoice.request}") private String topicInvoiceRequest;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate kafkaTemplate;

    public void sendInvoiceRequest(InvoiceRequest request) {
        try {
            String json = objectMapper.writeValueAsString(request);
            kafkaTemplate.send(topicInvoiceRequest, json);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }
}
