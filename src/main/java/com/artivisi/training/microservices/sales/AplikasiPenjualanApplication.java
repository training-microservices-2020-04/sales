package com.artivisi.training.microservices.sales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AplikasiPenjualanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiPenjualanApplication.class, args);
	}

}
