package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dto.ProductDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component @Slf4j
public class CatalogBackendServiceFallback implements CatalogBackendService{
    @Override
    public ProductDto findProductById(String id) {
        log.info("Fallback find product by id : {}", id);
        return null;
    }
}
