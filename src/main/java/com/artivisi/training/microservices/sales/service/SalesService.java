package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dao.CustomerDao;
import com.artivisi.training.microservices.sales.dao.SalesDao;
import com.artivisi.training.microservices.sales.dto.InvoiceRequest;
import com.artivisi.training.microservices.sales.entity.Customer;
import com.artivisi.training.microservices.sales.entity.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service @Transactional
public class SalesService {
    @Autowired private SalesDao salesDao;
    @Autowired private CustomerDao customerDao;
    @Autowired private KafkaSenderService kafkaSenderService;

    public void saveSales(Sales sales) {
        Customer c = customerDao.findById(sales.getCustomer().getId()).get();
        sales.setCustomer(c);
        salesDao.save(sales);

        InvoiceRequest invoiceRequest = new InvoiceRequest();
        invoiceRequest.setAmount(sales.getTotalAmount());
        invoiceRequest.setSalesNumber(sales.getSalesNumber());
        invoiceRequest.setCustomerEmail(sales.getCustomer().getEmail());
        invoiceRequest.setCustomerName(sales.getCustomer().getFullname());
        invoiceRequest.setSalesTime(sales.getTransactionTime());
        invoiceRequest.setDescription("Sales " + sales.getSalesNumber());

        kafkaSenderService.sendInvoiceRequest(invoiceRequest);
    }
}
