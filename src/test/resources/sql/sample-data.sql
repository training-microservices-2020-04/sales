insert into customer (id, email, fullname) values
('c001', 'customer001@yopmail.com', 'Customer 001');

insert into sales (id, transaction_time, sales_number, id_customer) values
('s001', '2021-01-05T08:59:59', 'S-001', 'c001');

insert into sales_detail (id, id_sales, product_code, unit_price, quantity) values
('sd001', 's001', 'p001', 100001, 3);